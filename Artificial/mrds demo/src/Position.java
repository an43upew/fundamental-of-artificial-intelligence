
public class Position
{
   private double x, y;

   public Position(double pos[])
   {
      this.x = pos[0];
      this.y = pos[1];
   }

   public Position(double x, double y)
   {
      this.x = x;
      this.y = y;
   }

   public double getX() { return x; }
   public double getY() { return y; }

   public double getDistanceTo(Position p)
   {
      return Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y));
   }

   // bearing relative 'north'
   public double getBearingTo(Position p)
   {
      return Math.atan2(p.y - y, p.x - x);
   }
   public double dot(Position p1){
	   return x*p1.x+y*p1.y;
   }
   public Position difference(Position p1){
	   return new Position(p1.x-x,p1.y-y);
   }
}
