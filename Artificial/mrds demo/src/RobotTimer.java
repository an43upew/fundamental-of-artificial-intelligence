import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RobotTimer {
	private long startTime;
	private String pathName;

	public RobotTimer(String pathName) {
		startTime = 0;
		this.pathName = pathName;
	}

	public void startTimer() {
		// Start Time
		startTime = System.nanoTime();
	}

	public void stopTimer() {
		// stop timer
		long estimatedTime = System.nanoTime() - startTime;
		double estimatedTimeinSeconds = (double) estimatedTime / 1000000000.0;

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date)); // 2014/08/06 15:59:48

		String output = pathName + " " + estimatedTimeinSeconds + " sec";
		System.out.println("Elapsed Time for path: " + output);

		output = dateFormat.format(date) + " " + output + "\n";

		try {
			Files.write(Paths.get("timeLog.txt"), output.getBytes(),
					StandardOpenOption.CREATE, StandardOpenOption.APPEND);
		} catch (IOException e) {
			// exception handling left as an exercise for the reader
		}
	}
}
