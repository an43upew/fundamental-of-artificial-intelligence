import java.io.FileReader;
import java.util.Vector;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class Path {
	
	private Vector<Position> path;
	
	/**
	 * Convert input path to Vector<Position>
	 * @param fileName: input path
	 */
	public Path(String fileName) throws Exception{
		JSONParser parser = new JSONParser();
		
		Object object = parser.parse(new FileReader(fileName));
		JSONArray jsonArray = (JSONArray) object;
		int length = jsonArray.size();

		//Orientation from the robot while driving through the logged path
		Vector<Quaternion> quarternionArray = new Vector<Quaternion>();
		//Position from the robot while driving through the logged path
		path = new Vector<Position>();
		for (int i = 0; i < length; i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.get(i);
			JSONObject jsonObjectPose = (JSONObject) jsonObject.get("Pose");
			JSONObject jsonObjectOrientation = (JSONObject) jsonObjectPose
					.get("Orientation");
			Quaternion q = new Quaternion(Double.valueOf(jsonObjectOrientation
					.get("W").toString()), Double.valueOf(jsonObjectOrientation
					.get("X").toString()), Double.valueOf(jsonObjectOrientation
					.get("Y").toString()), Double.valueOf(jsonObjectOrientation
					.get("Z").toString()));
			quarternionArray.add(q);
			JSONObject jsonObjectPos = (JSONObject) jsonObjectPose
					.get("Position");

			double x = Double.valueOf(jsonObjectPos.get("X").toString());
			double y = Double.valueOf(jsonObjectPos.get("Y").toString());
			//System.out.println("JSON X "+x+" Y "+y);
			
			Position pos = new Position(x, y);
			path.add(pos);
		}
	}
	/**
	 * return Position at index in path
	 * @param index
	 * @return
	 */
	public Position get(int index){
		return path.get(index);
	}
	
	/**
	 * return last path point
	 * @return
	 */
	public Position getLastPoint(){
		return path.get(path.size()-1);
	}
	
	/**
	 * return x coordinate of position at index
	 * @param index
	 * @return
	 */
	public double getX(int index){
		return path.get(index).getX();
	}
	/**
	 * return y coordinate of position at index
	 * @param index
	 * @return
	 */
	public double getY(int index){
		return path.get(index).getY();
	}
	/**
	 * return number of path points
	 * @return
	 */
	public int getCount(){
		return path.size()-1;
	}
	/**
	 * return current distance of robot to last point of path
	 * @param robot
	 * @return
	 */
	public double distanceRobotToLastPoint(Position robot){

		Position pathLastPosition = getLastPoint();

		
		double x_squared = Math.pow(pathLastPosition.getX()-robot.getX(), 2);
		double y_squared = Math.pow(pathLastPosition.getY()-robot.getY(), 2);
		return Math.sqrt(x_squared+y_squared);
	}
	
}
