import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Timer;
import java.util.Vector;

import javax.crypto.spec.PSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.omg.PortableInterceptor.DISCARDING;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * TestRobot interfaces to the (real or virtual) robot over a network
 * connection. It uses Java -> JSON -> HttpRequest -> Network -> DssHost32 ->
 * Lokarria(Robulab) -> Core -> MRDS4
 * 
 * @author thomasj
 */
public class TestRobot2 {
	private RobotCommunication robotcomm; // communication drivers
	private Carrot carrot;
	/**
	 * Create a robot connected to host "host" at port "port"
	 * 
	 * @param host
	 *            normally http://127.0.0.1
	 * @param port
	 *            normally 50000
	 */
	public TestRobot2(String host, int port) {
		robotcomm = new RobotCommunication(host, port);
	}

	/**
	 * This simple main program creates a robot, sets up some speed and turning
	 * rate and then displays angle and position for 16 seconds.
	 * 
	 * @param args
	 *            not used
	 * @throws Exception
	 *             not caught
	 */
	public static void main(String[] args) throws Exception {
		System.out.println("Creating Robot");
		TestRobot2 robot = new TestRobot2("http://127.0.0.1", 50000);

		robot.run();
	}
	
	/*CONSTANT PARAMETERS*/
	private final static double LINEARSPEED = 5;	
	//dampen the angular speed
	private final static double TURNINGQUOTIENT = 30;
	
	
	

	private void run() throws Exception {
		
		//Choose path:
		//String pathName = "Path-around-Table.json";
		//String pathName = "Path-to-bed.json";
		String pathName = "Path-around-Table-and-back.json";
		//String pathName = "Path-from-bed.json";

		//initialize path
		Path path = new Path(pathName);

		//initialize echo response
		LaserEchoesResponse le = new LaserEchoesResponse();

		System.out.println("Creating response");
		LocalizationResponse lr = new LocalizationResponse();

		System.out.println("Creating request");
		DifferentialDriveRequest dr = new DifferentialDriveRequest();



		System.out.println("Start to move robot");
		int rc = robotcomm.putRequest(dr);
		System.out.println("Response code " + rc);
		
		Boolean run = true;

		double[] positionRobot;
	
		RobotTimer timer = new RobotTimer(pathName);
		
		carrot=new Carrot();
		
		timer.startTimer();
		while(run){
			
			// ask the robot about its position and angle
			robotcomm.getResponse(lr);

			//convert robot angle to degree
			double robotAngle = lr.getHeadingAngle()* 180 / 3.1415926;
			
			///current robot position
			positionRobot = getPosition(lr);
			Position robotPosition= new Position(positionRobot[0], positionRobot[1]);
			
			//ask for current echoes
			robotcomm.getResponse(le);
			Vector<Double> echoes = le.getVectorEchoes();
			
			//compute next carrot point
			carrot.computeNextCarrotPoint(path, robotPosition, echoes);
			
			//get angle to carrot point
			double angleToCarrotPoint = carrot.getAngleToCarrotPoint(robotPosition);
			
			//compute new angular speed for the robot
			double newAngularSpeed = getNewAngularSpeed(angleToCarrotPoint, robotAngle);
			

			
			dr.setAngularSpeed(newAngularSpeed/TURNINGQUOTIENT);
			
			//factor to slow down while turning
			double angleRelation = Math.min(1,(Math.min(Math.abs(angleToCarrotPoint),Math.abs(robotAngle))/Math.max(Math.abs(angleToCarrotPoint),Math.abs(robotAngle))));
			//factor to slow down depending on distance to carrot point. Distance is low if the carrot beta is high meaning, that there is a object nearby
			double distanceFactor =  Math.min(1,carrot.getDistanceToCarrotPoint(robotPosition));
			
			dr.setLinearSpeed(LINEARSPEED  * distanceFactor + angleRelation);
			
			rc = robotcomm.putRequest(dr);
			
			
			double distanceRobotLastPoint = path.distanceRobotToLastPoint(robotPosition);			
			//condition for finish: robot is in the last third part of the path and distance to last point is less equal 1
			if(path.getCount()- carrot.getCurrentStartIndex() < ((int)path.getCount()*0.3) && distanceRobotLastPoint <= 1)
				run =false;
			
			System.out.println(" ");
			
			
		}
		timer.stopTimer();
		
		// set up request to stop the robot
		dr.setLinearSpeed(0);
		dr.setAngularSpeed(0);

		System.out.println("Stop robot");
		rc = robotcomm.putRequest(dr);
		System.out.println("Response code " + rc);
		
		

	

	}

	/**
	 * Extract the robot bearing from the response
	 * 
	 * @param lr
	 * @return angle in degrees
	 */
	double getBearingAngle(LocalizationResponse lr) {
		double e[] = lr.getOrientation();

		double angle = 2 * Math.atan2(e[3], e[0]);
		return angle * 180 / Math.PI;
	}
	

	/**
	 * Extract the position
	 * 
	 * @param lr
	 * @return coordinates
	 */
	double[] getPosition(LocalizationResponse lr) {
		return lr.getPosition();
	}
	/**
	 * compute and return angular speed based on angle to carrot point and robot angle
	 * @param angleToCarrotPoint
	 * @param robotAngle
	 * @return
	 */
	double getNewAngularSpeed(double angleToCarrotPoint, double robotAngle){
		double newAngularSpeed;
		
		//carrot point is left to current robot angle
		if (angleToCarrotPoint<robotAngle) {
			newAngularSpeed=-(robotAngle-angleToCarrotPoint);
			//correct angle in order to go over the -180/180 border
			if((newAngularSpeed>90 || newAngularSpeed < -90)) newAngularSpeed = 180 - robotAngle + 180 + angleToCarrotPoint;
		}
		//carrot point is right to current robot angle
		else {
			newAngularSpeed=angleToCarrotPoint-robotAngle;
			//correct angle in order to go over the -180/180 border
			if((newAngularSpeed>90 || newAngularSpeed < -90)) newAngularSpeed = -(180+(180-angleToCarrotPoint)+robotAngle);
		}
		return newAngularSpeed;
	}

}
