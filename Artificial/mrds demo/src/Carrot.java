import java.util.Vector;

public class Carrot {
	private static final double LOOKAHEADDISTANCE = 1.5;
	//offset to increase the search area for carrot point
	private static final double ALPHA = 0.5;
	//offset to decrease the search are for carrot point in case of object echo
	private static final double BETA = 0;
	//distance to detect object
	private static final double ECHODISTANCE = 0.5;

	// from this index the search for the right path segment will start
	private int currentStartIndex;

	private Position carrotPosition;
	
	/**
	 * initiate carrot, set current start index to 0
	 */
	public Carrot(){
		currentStartIndex=0;
	}

	/**
	 * compute next carrot point on path based on current robot position and recieved echoes
	 * @param path
	 * @param robotPos
	 * @param echoes
	 */
	public void computeNextCarrotPoint(Path path, Position robotPos, Vector<Double> echoes) {
		int p0 = currentStartIndex;

		double alpha = ALPHA;
		double beta = BETA;
		
		
		boolean foundCarrotPoint = false;
		while (!foundCarrotPoint) {
			
			for (int i = currentStartIndex; i <path.getCount(); i++) {
				double newDistance = getDistance(robotPos, path.get(i),
						path.get(i + 1));
				
				//traverse echoes between 100 and 160 to detect objects in the front area of the robot
				for (int j = 100; j < 160; j++) {
					if (echoes.get(j)<ECHODISTANCE) {
						//if object is detected then decrease search area by increasing beta
						beta = Math.min(2,beta+0.25);
						//System.out.println("Beta: " + beta);
						break;
					}
				}
				//look for distance in certain area of LOOKAHEADDISTANCE
				if ((LOOKAHEADDISTANCE - alpha - beta) < newDistance
						&& (LOOKAHEADDISTANCE + alpha - beta) > newDistance) {
					p0 = i;
					foundCarrotPoint = true;


					//System.out.println("Nearest Segment: " + p0 + " " + p0+1
					//		+ " Distance: " + newDistance);
					break;
					
				}
				
			}
			//if no carrot point found then increase search area by increasing alpha
			if(!foundCarrotPoint)
				alpha += 0.5;
			//System.out.println("Alpha: " + alpha);	
			
		}
		alpha = ALPHA;
		beta = BETA;
		
		//remember current index in order to start next carrot point search at this index
		currentStartIndex = p0;
		//System.out.println("Next Carrot Position: X " + carrotPosition.getX() +" Y "+carrotPosition.getY());
	}
	/**
	 * return shortest distance between robot position and current path segment
	 * assign this point as carrot point
	 * @param robotPos
	 * @param p0
	 * @param p1
	 * @return
	 */
	private double getDistance(Position robotPos, Position p0, Position p1) {
		Position v = p0.difference(p1); // used in the course
		Position w = p0.difference(robotPos); // used in the course
		double c1 = w.dot(v);
		double c2 = v.dot(v);
		
		//Point 0 is closer
		if (c1 <= 0) {	
			carrotPosition = p0;
			return p0.getDistanceTo(robotPos);
			//Point 1 is closer
		} else if (c2 <= c1) {
			carrotPosition = p1;
			return p1.getDistanceTo(robotPos);
			//Point on Path is closer
		} else {
			double b = c1 / c2;
			carrotPosition = new Position(p0.getX() + v.getX() * b, p0.getY()
					+ v.getY() * b);
			return carrotPosition.getDistanceTo(robotPos);
		}
	}
	/**
	 * get current start index
	 * @return
	 */
	public int getCurrentStartIndex(){
		return currentStartIndex;
	}
	/**
	 * get angle between current robot orientation and carrot point
	 * @param robot
	 * @return
	 */
	public double getAngleToCarrotPoint(Position robot){
		return Math.atan2(carrotPosition.getY()-robot.getY(), carrotPosition.getX()-robot.getX())* 180 / 3.1415926;
	}
	/**
	 * get distance between current robot position and carrot point
	 * @param robot
	 * @return
	 */
	public double getDistanceToCarrotPoint(Position robot){
		return carrotPosition.getDistanceTo(robot); 
	}
}
