import java.util.Random;


public class Neuron {
	//weight matrix, same data structure as image for simplicity reasons
	public double[][] weight = new double[20][20];
	//value of extra node, always 1
	private double extraNode=1;
	//weight of extra node, also called bias
	private double bias = -18;
	//learning rate
	private double alpha=0.1;
	//facit of this neuron depending on which emotion it should recognize
	private int facit;
	/**
	 * initialize neuron with emotion(facit)
	 * @param facit
	 */
	public Neuron(int facit){
		this.facit= facit;
		
		//initialize weights with random numbers [-1,+1]
		Random random = new Random();
		 for(int i = 0; i < weight.length; i++) {
				for (int j = 0; j <weight.length; j++) {
					weight[i][j]=random.nextDouble()*2-1;
				}
			}		
	}
	
	/**
	 * sigmoid function
	 * @param x
	 * @return sigmoid(x)
	 */
	private double sigmoid(double x){
		return 1.0/(1.0+(Math.exp(-x)));
	}

	/**
	 * compute activation level of sum of image with bias from extra Node
	 * @param image
	 * @return
	 */
	public double activate(Image image){
		return sigmoid(sum(image)+bias*extraNode);
	}
	
	/**
	 * compute sum of image with corresponding weights
	 * @param image
	 * @return
	 */
	public double sum(Image image){
		double sum=0;
		for (int i = 0; i < weight.length; i++) {
			for (int j = 0; j <weight.length; j++) {
				sum +=weight[i][j]*image.matrix[i][j];
			}
		}
		return sum;
	}
	
	/**
	 * adjust weights depending on current error
	 * @param image
	 */
	public void adjustWeights(Image image){
		double activation = activate(image);
		//determine if current neuron should recognize image as true or false
		int desiredOutput = facit == image.facit ? 1 : 0;
		double error = desiredOutput - activation;
		for (int i = 0; i < weight.length; i++) {
			for (int j = 0; j <weight.length; j++) {
				double deltaWeight = alpha * error * image.matrix[i][j];
				weight[i][j]+=deltaWeight;
			}
		}
		
		
		
	}
	/**
	 * return facit of this neuron
	 * @return
	 */
	public int getFacit(){
		return facit;
	}
}
