
public class Image {
	//image name
	public String name;
	//image facit
	public int facit;
	//image values in matrix data structure
	public double[][] matrix = new double[20][20];
	//counter for row filling
	private int currentRow;
	
	/**
	 * initialize image with image name and set currentRow to 0
	 * @param name
	 */
	public Image(String name){
		this.name = name;
		currentRow = 0;
	}
	/**
	 * parse string array to image values and save in matrix
	 * scale to [0,1]
	 * @param line
	 */
	public void add(String[] line){
		for(int i=0;i<line.length;i++){
			matrix[currentRow][i] = (double)Double.parseDouble(line[i])/31.0;
		}
		currentRow++;
	}
	/**
	 * print out image matrix
	 */
	public String toString(){
		String s=name+"\n"+"Facit: "+facit+"\n";
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				s=s+" "+matrix[i][j];
			}
			s=s+"\n";
		}
		return s;
	}
}
