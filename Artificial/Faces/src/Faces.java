import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Vector;

public class Faces {

	public static void main(String[] args) throws Exception {

		//initialize filereader
		FileReader fr = new FileReader(args[0]);
		BufferedReader br = new BufferedReader(fr);

		Vector<Image> training = new Vector<Image>();
		
		
		//convert training.txt to images, later used as training set
		String line;
		while ((line = br.readLine()) != null) {
			if (line.startsWith("#"))
				continue;
			if (line.isEmpty())
				continue;

			// magic from stackoverflow
			// http://stackoverflow.com/questions/5238491/check-if-string-contains-only-letters
			if (Character.isLetter(line.charAt(0))) {
				Image i = new Image(line);
				training.add(i);
				continue;
			}
			// magic from stackoverflow
			// http://stackoverflow.com/questions/18590901/check-if-a-string-contains-numbers-java
			if (line.matches(".*\\d+.*")) {
				String[] parts = line.split(" ");
				training.get(training.size() - 1).add(parts);
			}
		}

		br.close();
		fr.close();
		line = "";

		fr = new FileReader(args[1]);
		br = new BufferedReader(fr);

		//convert facit.txt and add to image facits in training set
		while ((line = br.readLine()) != null) {
			if (line.startsWith("#"))
				continue;
			if (line.isEmpty())
				continue;

			// magic from stackoverflow
			// http://stackoverflow.com/questions/5238491/check-if-string-contains-only-letters
			if (Character.isLetter(line.charAt(0))) {
				String[] parts = line.split(" ");
				for (int i = 0; i < training.size(); i++) {
					if (training.get(i).name.equals(parts[0])) {
						training.get(i).facit = Integer.parseInt(parts[1]);
					}
				}
			}

		}

		br.close();
		fr.close();

		line = "";
		
		Vector<Image> test = new Vector<Image>();
		fr = new FileReader(args[2]);
		br = new BufferedReader(fr);
		//convert test.txt to images later used as test set
		while ((line = br.readLine()) != null) {
			if (line.startsWith("#"))
				continue;
			if (line.isEmpty())
				continue;

			// magic from stackoverflow
			// http://stackoverflow.com/questions/5238491/check-if-string-contains-only-letters
			if (Character.isLetter(line.charAt(0))) {
				Image i = new Image(line);
				test.add(i);
				continue;
			}
			// magic from stackoverflow
			// http://stackoverflow.com/questions/18590901/check-if-a-string-contains-numbers-java
			if (line.matches(".*\\d+.*")) {
				String[] parts = line.split(" ");
				test.get(test.size() - 1).add(parts);
			}
		}
		br.close();
		fr.close();
		
		
		//initialize Neuronal Net
		NeuronalNet neuronalNet = new NeuronalNet();

		//train all neurons 70 times
		neuronalNet.trainNeuronsNTimes(training, 70);

		//test trained neurons with test set
		neuronalNet.testNeurons(test);

	}
}
