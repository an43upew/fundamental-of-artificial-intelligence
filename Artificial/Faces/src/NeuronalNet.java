import java.util.Collections;
import java.util.Vector;


public class NeuronalNet {
	//declare and initialize neurons, one for each emotion
	Vector<Neuron> neuronalNet= new Vector<Neuron>();
	Neuron happy= new Neuron(1);
	Neuron sad= new Neuron(2);
	Neuron mischievous= new Neuron(3);
	Neuron mad= new Neuron(4);

	
	/**
	 * add each neuron to neuronal Net Vector
	 */
	public NeuronalNet(){

		neuronalNet.add(happy);
		neuronalNet.add(sad);
		neuronalNet.add(mischievous);
		neuronalNet.add(mad);

	}
	/**
	 * train all neurons over all images
	 * shuffle training images order for better result
	 * @param training
	 */
	public void trainNeurons(Vector<Image> training){
		
		//shuffle before training
		Collections.shuffle(training);
		for (int i = 0; i < neuronalNet.size(); i++) {
			for (int j = 0; j < training.size(); j++) {
				neuronalNet.get(i).adjustWeights(training.get(j));
			}
		}
	}
	/**
	 * train training set n times
	 * @param training
	 * @param n
	 */
	public void trainNeuronsNTimes(Vector<Image>training,int n){
		for (int i = 0; i < n; i++) {
			trainNeurons(training);
		}
	}
	
	/**
	 * test neurons with test set
	 * print out image name and neuron with highest activation level
	 * @param test
	 */
	public void testNeurons(Vector<Image> test){
		
		//facit matrix, column 0 facit for this image if available. Was used to compare results during development
		//column 1-4 activation level from corresponding neuron
		//one row for each image in test set
		double[][] facit = new double[test.size()][5];
		
		Vector<Integer> neuronFired = new Vector<Integer>();
		//iterate over all neurons
		for (int i = 0; i < neuronalNet.size(); i++) {
			//fill facit matrix with facits and activation levels
				for (int j = 0; j < test.size(); j++) {	
				facit[j][0] = test.get(j).facit;
				facit[j][i+1] = (double) neuronalNet.get(i).activate(test.get(j));
			
			}
		}
		//iterate over facit matrix and save neuron with highest activation level for each image
		for (int i = 0; i < facit.length; i++) {
			double max =0;
			int neuron = -1;
			for (int j = 1; j < facit[0].length; j++) {
				if(max<facit[i][j]){
					max = facit[i][j];
					neuron = j;
					
				}
			}
			neuronFired.add(neuron);
			
			
		}
		//print out image name and activated neuron
		for (int i = 0; i < test.size(); i++) {
			System.out.println(test.get(i).name +" "+neuronFired.get(i));
		}
		
	}

}
